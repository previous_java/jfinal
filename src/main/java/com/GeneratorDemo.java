package com;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import com.jfinal.kit.PathKit;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.generator.Generator;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.xyz.utils.MyMetaBuilder;

/**
 * GeneratorDemo
 */
public class GeneratorDemo {
	
	public static DataSource getDataSource() {
		Prop p = PropKit.use("dbConfig.txt");
		C3p0Plugin c3p0Plugin = new C3p0Plugin(p.get("jdbcUrl"), p.get("user"), p.get("password"));
		c3p0Plugin.start();
		return c3p0Plugin.getDataSource();
	}
	
	public static void main(String[] args) {
		String[] prefixes = {"ac", "common", "m", "course", "ac", "sys", "user"};
		for (String prefix : prefixes) {
			// 定义一个 
			Map<String, String> packageName = new HashMap<String, String>();
			// base model 所使用的包名
			String baseModelPackageName = "com.xyz.drp.model." + prefix + ".base";
			// base model 文件保存路径
			String baseModelOutputDir = PathKit.getWebRootPath() + "/src/main/java/com/xyz/drp/model/" + prefix + "/base";
			System.out.println(baseModelOutputDir);
			// model 所使用的包名 (MappingKit 默认使用的包名)
			String modelPackageName = "com.xyz.drp.model." + prefix;
			// model 文件保存路径 (MappingKit 与 DataDictionary 文件默认保存路径)
			String modelOutputDir = baseModelOutputDir + "/../";
			// 创建生成器
			Generator gernerator = new Generator(getDataSource(), baseModelPackageName, baseModelOutputDir, modelPackageName, modelOutputDir);
			// 添加不需要生成的表名
//			gernerator.addExcludedTable("adv");
			gernerator.setMetaBuilder(new MyMetaBuilder(getDataSource()));
			gernerator.setTablePrefix("g_" + prefix);
			// 只对指定前缀的表
			// 设置是否在 Model 中生成 dao 对象
			gernerator.setGenerateDaoInModel(true);
			// 设置是否生成字典文件
			gernerator.setGenerateDataDictionary(true);
			// 设置需要被移除的表名前缀用于生成modelName。例如表名 "osc_user"，移除前缀 "osc_"后生成的model名为 "User"而非 OscUser
			gernerator.setRemovedTableNamePrefixes("g_");
			// 生成
			gernerator.generate();
		}
	}
}




