/**
 * Copyright (c) 2015-2016, Michael Yang 杨福海 (fuhai999@gmail.com).
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.xyz;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import com.jfinal.config.Constants;
import com.jfinal.core.Const;
import com.jfinal.render.FreeMarkerRender;
import com.jfinal.render.RenderException;

import freemarker.template.Template;

public class JMFreeMarkerRender extends FreeMarkerRender {

	public JMFreeMarkerRender(String view) {
		super(view);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void render() {

		Map<String, Object> jpTags = new HashMap<String, Object>();
		jpTags.putAll(JMainTags.jmTags);
		Map data = new HashMap();
		for (Enumeration<String> attrs = request.getAttributeNames(); attrs.hasMoreElements();) {
			String attrName = attrs.nextElement();
			if (attrName.startsWith("jm.")) {
				jpTags.put(attrName.substring(3), request.getAttribute(attrName));
			} else {
				data.put(attrName, request.getAttribute(attrName));
			}
		}
		data.put("jm", jpTags);

		String htmlContent;
		try {			
			htmlContent = getHtmlContent(data);
		} catch (Exception e) {
			htmlContent = get404HtmlContent(data);
		}

		WriterHtml(htmlContent, getContentType(), false);
	}

	private void WriterHtml(String htmlContent, String contentType, boolean isPutToCache) {
		response.setContentType(contentType);
		PrintWriter responseWriter = null;
		try {
			responseWriter = response.getWriter();
			responseWriter.write(htmlContent);
		} catch (Exception e) {
			if (Const.DEFAULT_DEV_MODE) {
				e.printStackTrace();
			}
			throw new RenderException(e);
		} finally {
			close(responseWriter);
		}
	}

	@SuppressWarnings("rawtypes")
	private String getHtmlContent(Map data) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		OutputStreamWriter osWriter = null;
		try {
			osWriter = new OutputStreamWriter(baos, Const.DEFAULT_ENCODING);
			Template template = getConfiguration().getTemplate(view);
			template.process(data, osWriter);
			osWriter.flush();
			return baos.toString(Const.DEFAULT_ENCODING);
		} catch (Exception e) {
			if (Const.DEFAULT_DEV_MODE) { // 如果是开发模式那么打印堆栈信息
				e.printStackTrace();
			}
			throw new RenderException(e);
		} finally {
			close(baos);
			close(osWriter);
		}
	}
	@SuppressWarnings("rawtypes")
	private String get404HtmlContent(Map data) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		OutputStreamWriter osWriter = null;
		try {
			osWriter = new OutputStreamWriter(baos, Const.DEFAULT_ENCODING);
			Template template = getConfiguration().getTemplate(JMainTags.html404);
			template.process(data, osWriter);
			osWriter.flush();
			return baos.toString(Const.DEFAULT_ENCODING);
		} catch (Exception e) {
			if (Const.DEFAULT_DEV_MODE) { // 如果是开发模式那么打印堆栈信息
				e.printStackTrace();
			}
			throw new RenderException(e);
		} finally {
			close(baos);
			close(osWriter);
		}
	}

	private void close(Writer writer) {
		if (writer != null) {
			try {
				writer.close();
			} catch (IOException e) {
			}
		}
	}

	private void close(OutputStream stream) {
		if (stream != null) {
			try {
				stream.close();
			} catch (IOException e) {
			}
		}
	}

}
