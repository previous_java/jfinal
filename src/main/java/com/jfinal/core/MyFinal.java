package com.jfinal.core;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

/**
 * 将actionKey都取出来
 * @author xyz
 *
 */
public class MyFinal {

	public static String initActionMapping() {
		
		String strResult = "";
		int i = 1;
		Set<Entry<String, Class<? extends Controller>>> entrySet = Config.getRoutes().getEntrySet();
		for (Iterator<Entry<String, Class<? extends Controller>>> iterator = entrySet.iterator(); iterator.hasNext();) {
			Entry<String, Class<? extends Controller>> entry = (Entry<String, Class<? extends Controller>>) iterator
					.next();
			// 输出所有ControllerKey
//			System.out.println("" + entry.getKey());
			// 取出所有操作uri
			Class<? extends Controller> clazz = entry.getValue();
			Method[] methods = clazz.getDeclaredMethods();
			for (Method md : methods) {

				Annotation[] a = md.getAnnotations();
				for (Annotation an : a) {
					if (an instanceof Urls)
					{
						strResult += i++ + "、    url：" + entry.getKey() + "/" + md.getName();
						/*strResult += "\t菜单名称：" + ((Urls)an).name();
						strResult += "\t是否显示：" + ((Urls)an).display();*/
						strResult += "\t" + ((Urls)an).des();
						strResult  += "\n";
					} /*else {
						strResult += "\t" + "url：" + entry.getKey() + "/" + md.getName() + "\n";
					}*/
				}
			}
			strResult  += "\n";
		}
		return strResult;
	}
}
