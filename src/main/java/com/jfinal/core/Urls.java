package com.jfinal.core;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Urls {

	String name() default ""; // 菜单名称
	byte display() default 0; // 是否显示，默认不显示
	String des(); // 描述
}
